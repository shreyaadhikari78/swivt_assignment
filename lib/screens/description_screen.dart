import 'package:assignment/screens/home_screen.dart';
import 'package:flutter/material.dart';

class DescriptionScreen extends StatefulWidget {
  final String description;

  const DescriptionScreen({Key? key, required this.description})
      : super(key: key);

  @override
  _DescriptionScreenState createState() => _DescriptionScreenState();
}

class _DescriptionScreenState extends State<DescriptionScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: GestureDetector(
            onTap: () {
              Navigator.pop(context);

            },
            child: const Icon(Icons.arrow_back_ios)),
        title: const Text("Description"),
        centerTitle: true,
      ),
      body: Center(child: Text(widget.description)),
    );
  }
}
