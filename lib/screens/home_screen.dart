import 'package:assignment/bloc/posts_bloc/posts_bloc.dart';
import 'package:assignment/bloc/posts_bloc/posts_event.dart';
import 'package:assignment/bloc/posts_bloc/posts_state.dart';
import 'package:assignment/repository/posts_repository.dart';
import 'package:assignment/screens/description_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return  _postBody();
  }

  Widget _postBody(){
    return BlocProvider(
      create: (context) => PostsBloc(
        RepositoryProvider.of<PostsRepository>(context),
      )..add(LoadPostsEvent()),
      child: Scaffold(
        appBar: AppBar(
          title: const Text('The Post App'),
          centerTitle: true,
        ),
        body: BlocBuilder<PostsBloc, PostsState>(
          builder: (context, state) {
            if (state is PostsLoadingState) {
              return const Center(
                child: CircularProgressIndicator(),
              );
            }
            if (state is PostsLoadedState) {
              return Center(
                child: SingleChildScrollView(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      ListView.builder(
                        shrinkWrap: true,
                        physics:const NeverScrollableScrollPhysics(),
                        itemCount: state.posts.posts.length,
                        itemBuilder: (context,int index) {
                          return GestureDetector(
                            onTap: (){
                              Navigator.pushReplacement(
                                  context,
                                  MaterialPageRoute(
                                      builder: (BuildContext context) =>
                                          DescriptionScreen(
                                            description:
                                           state.posts.posts[index].body,
                                          )));
                            },
                            child: Card(
                              elevation: 20,
                              child: SizedBox(
                                height: 50.0,
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Text(
                                    state.posts.posts[index].title,
                                    textAlign: TextAlign.center,
                                  ),
                                ),


                              ),
                            ),
                          );
                        },
                      ),

                    ],
                  ),
                ),
              );
            }
            if (state is PostsErrorState) {
              return Center(
                child: Text(state.error.toString()),
              );
            }
            return Container();
          },
        ),
      ),
    );
  }
}