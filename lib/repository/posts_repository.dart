import 'dart:convert';

import 'package:assignment/models/posts_model.dart';
import 'package:http/http.dart' as http;

class PostsRepository {
  final String _baseUrl = "https://dummyjson.com/posts";

  Future<Posts> getPosts() async {
    final response = await http.get(Uri.parse(_baseUrl));
    if (response.statusCode == 200) {
      return Posts.fromJson(jsonDecode(response.body));
    } else {
      throw Exception("Failed to load posts");
    }
  }
}