import 'package:assignment/models/posts_model.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';




@immutable
abstract class PostsState extends Equatable {}

class PostsLoadingState extends PostsState {
  @override
  List<Object?> get props => [];
}

class PostsLoadedState extends PostsState {
  final Posts posts;

  PostsLoadedState(this.posts);

  @override
  List<Object?> get props => [posts];
}

class PostsErrorState extends PostsState {
  final String error;

  PostsErrorState(this.error);

  @override
  List<Object?> get props => [error];
}