import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

@immutable
abstract class PostsEvent extends Equatable {
  const PostsEvent();
}

class LoadPostsEvent extends PostsEvent {
  @override
  List<Object> get props => [];
}