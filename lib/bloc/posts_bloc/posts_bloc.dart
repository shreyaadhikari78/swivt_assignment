import 'package:assignment/bloc/posts_bloc/posts_event.dart';
import 'package:assignment/bloc/posts_bloc/posts_state.dart';
import 'package:assignment/repository/posts_repository.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class PostsBloc extends Bloc<PostsEvent, PostsState> {
  final PostsRepository _postsRepository;

  PostsBloc(this._postsRepository) : super(PostsLoadingState()) {
    on<LoadPostsEvent>((event, emit) async {
      emit(PostsLoadingState());
      try {
        final joke = await _postsRepository.getPosts();
        emit(PostsLoadedState(joke));
      } catch (e) {
        emit(PostsErrorState(e.toString()));
      }
    });
  }
}